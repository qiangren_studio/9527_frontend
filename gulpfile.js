var gulp = require('gulp');
var browserSync = require('browser-sync');
var htmlmin = require('gulp-htmlmin');
var cleanCSS = require('gulp-clean-css');
var uglify = require('gulp-uglify');
var imagemin = require('gulp-imagemin');
var gutil = require('gulp-util');


gulp.task('browser-sync', function () {
    browserSync.init({
        files: ['**'],
        server: {
            baseDir: './dist',  // 设置服务器的根目录
            index: './admin/index/index.html' // 指定默认打开的文件
        },
        port: 3000  // 指定访问服务器的端口号
    });
});

/*html压缩*/
gulp.task('htmlmin', function () {
    var options = {
        removeComments: true,//清除HTML注释
        collapseWhitespace: true,//压缩HTML
        collapseBooleanAttributes: true,//省略布尔属性的值 <input checked="true"/> ==> <input />
        removeEmptyAttributes: true,//删除所有空格作属性值 <input id="" /> ==> <input />
        removeScriptTypeAttributes: true,//删除<script>的type="text/javascript"
        removeStyleLinkTypeAttributes: true,//删除<style>和<link>的type="text/css"
        minifyJS: true,//压缩页面JS
        minifyCSS: true//压缩页面CSS
    };
    gulp.src('src/admin/**/*.html')
        .pipe(htmlmin(options))
        .pipe(gulp.dest('dist/admin'));
});

/*css压缩*/
gulp.task('cssmin', function () {
    gulp.src('src/static/admin/css/**/*.*')
        .pipe(cleanCSS({
            keepSpecialComments: '*'
        }))
        .pipe(gulp.dest('dist/static/admin/css'));
});

/*js压缩*/
gulp.task('jsmin', function () {
    gulp.src('src/static/admin/js/**/*.js')
        .pipe(uglify())
        .on('error', function (err) {
            gutil.log(gutil.colors.red('[Error]'), err.toString());
        })
        .pipe(gulp.dest('dist/static/admin/js'));
});

/*image压缩*/
gulp.task('imagesmin', function () {
    gulp.src('src/static/admin/images/**/*.*')
        .pipe(imagemin({
            optimizationLevel: 4 //类型：Number  默认：3  取值范围：0-7（优化等级）
        }))
        .pipe(gulp.dest('dist/static/admin/images'))
});

gulp.task('copy', function () {
    gulp.src('src/static/admin/api/*.*')
        .pipe(gulp.dest('dist/static/admin/api'));
    gulp.src('src/static/admin/fonts/*.*')
        .pipe(gulp.dest('dist/static/admin/fonts'));
    gulp.src('src/static/admin/layui/**/*.*')
        .pipe(gulp.dest('dist/static/admin/layui'));
    gulp.src('src/static/admin/lib/**/*.*')
        .pipe(gulp.dest('dist/static/admin/lib'));
});
gulp.task('build', ['htmlmin', 'cssmin', 'jsmin', 'imagesmin', 'copy']);
gulp.task('default', ['browser-sync']); 