layui.use(['layer', 'form', 'element', 'jquery'], function () {
	var layer = layui.layer;
	var element = layui.element;
	var form = layui.form;
	var $ = layui.jquery;
	var hideBtn = $('#hideBtn');
	var mainLayout = $('#main-layout');
	var mainMask = $('.main-mask');
	var user = null;
	var status = null;
	var exampass = null;
	var isLogin = getCookie('session_cid');
	var role = getCookie("role");
	var user_registry = 1;
	var user_role = 2;
	var user_exam = 1;
	if (isLogin == null) {
		location.href = "../../admin/index/login.html";
	}
	if (role == 1) {
		$.ajax({
			type: "POST",
			url: HOST + '/index/index',
			data: { "role": role, "session_cid": isLogin },
			success: function (res) {
				var data = JSON.parse(res);
				user_registry = data.registry;
				user_role = data.role;
				user_exam = data.exam;
				$('#user_info').html(data.name);

				/**
				 *  exam1为考试通过2为未通过
				*  role1为保险经纪人 2为天然气公司 3为保险公司
				*  register 1为通过2为未通过3为待审核
				*/
				if (user_role == 1) {
					$('.agent').show();
					$('.more').show();
					$('.natgas').remove();
					$('.insurance').remove();
					if (user_exam == 2) {
						$('.approve').hide();
					} else {
						$('.exam').hide();
					}
					if (user_registry == 1) {
						$('.exam').hide();
						$('.approve').hide();
					} else {
						$('.approvepass').hide();
					}
					if (user_registry == 1 && user_exam == 1) {
						$('.more').hide();
					}
				}
			},
			error: function (err) {
				layer.msg('服务器出错，请刷新重新');
			}
		});
	} else if (role == 2) {
		$.post({
			url: HOST + '/gas/self_info',
			data: { "comid": isLogin },
			dataType: 'json',
			success: function (res) {
				$('#user_info').html(res.cname);
			},
			error: function (err) {
				layer.msg("服务器出错");
			}
		})
		$('.natgas').show();
		$('.agent').remove();
		$('.insurance').remove();
		$('.more').remove();

	} else if (role == 3) {
		$('.insurance').show();
		$('.natgas').remove();
		$('.agent').remove();
		$('.more').remove();
	}
	function goExamOpen() {
		layer.open({
			type: 1
			, title: false
			, closeBtn: false
			, area: '300px;'
			, shade: .8
			, id: 'LAY_layuipro'
			, resize: false
			, btn: ['确认考试', '再想想']
			, btnAlign: 'c'
			, moveType: 1
			, content: '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;text-align:left;">考试时间为20分钟<br><br>共10道题目<br>其中5道单选，5道多选</div>'
			, success: function (layero) {
				var btnList = layero.find('.layui-layer-btn');
				var btn1 = btnList.find('.layui-layer-btn0');
				var btn2 = btnList.find('.layui-layer-btn1');
				btn1.click(function () {
					var isActive = $('.main-layout-tab .layui-tab-title').find("li[lay-id=1]");
					if (isActive.length > 0) {
						//切换到选项卡
						element.tabChange('tab', 1);
					} else {
						element.tabAdd('tab', {
							title: '在线考试',
							content: '<iframe src="../../admin/agent/exam.html" name="iframe1" class="iframe" framborder="0" data-id="1" scrolling="auto" width="100%"  height="100%"></iframe>',
							id: 1
						});
						element.tabChange('tab', 1);
					}

				})
				btn2.click(function () {
					return false;
				})
			}
		});
	}


	$('.more').click(function () {
		layer.open({
			type: 1
			, title: false //不显示标题栏
			, closeBtn: false
			, area: '300px;'
			, shade: .8
			, id: 'more' //设定一个id，防止重复弹出
			, resize: false
			, btn: ['去考试', '再想想']
			, btnAlign: 'c'
			, moveType: 1 //拖拽模式，0或者1
			, content: '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;text-align:left;">更多功能暂未开放<br><br>请先完成执业认证<br>1.通过考试<br>2.身份认证</div>'
			, success: function (layero) {
				var btnList = layero.find('.layui-layer-btn');
				var btn1 = btnList.find('.layui-layer-btn0');
				var btn2 = btnList.find('.layui-layer-btn1');
				btn1.click(function () {
					if (user_exam == 1) {
						layer.msg('您已经通过了考试，请完成执业认证');
						return false;
					} else {
						goExamOpen();
					}

				})
				btn2.click(function () {
					return false;
				})
			}
		});
	})
	//监听导航点击
	element.on('nav(leftNav)', function (elem) {
		var navA = $(elem);
		var id = navA.attr('data-id');
		var url = navA.attr('data-url');
		var text = navA.attr('data-text');
		var user = null;

		if (!url) {
			return;
		}
		var isActive = $('.main-layout-tab .layui-tab-title').find("li[lay-id=" + id + "]");
		if (isActive.length > 0) {
			//切换到选项卡
			element.tabChange('tab', id);
		} else {
			if (id == 1) {
				goExamOpen();
			} else if (id == 3) {
				if (user_registry == 3) {
					layer.open({
						type: 1
						, title: false
						, closeBtn: false
						, area: '300px;'
						, shade: .8
						, id: 'LAY_layuipro'
						, resize: false
						, btn: ['确定']
						, btnAlign: 'c'
						, moveType: 1
						, content: '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;text-align:left;">您的资格认证已经提交<br><br>请耐心等候结果</div>'
						, success: function (layero) {
							var btnList = layero.find('.layui-layer-btn');
							var btn1 = btnList.find('.layui-layer-btn0');
							btn1.click(function () {
								return false;
							})
						}
					});
				} else {
					element.tabAdd('tab', {
						title: text,
						content: '<iframe src="' + url + '" name="iframe' + id + '" class="iframe" framborder="0" data-id="' + id + '" scrolling="auto" width="100%"  height="100%"></iframe>',
						id: id
					});
					element.tabChange('tab', id);
				}
			}
			else {
				element.tabAdd('tab', {
					title: text,
					content: '<iframe src="' + url + '" name="iframe' + id + '" class="iframe" framborder="0" data-id="' + id + '" scrolling="auto" width="100%"  height="100%"></iframe>',
					id: id
				});
			}
			element.tabChange('tab', id);
		}
		mainLayout.removeClass('hide-side');
	});
	//监听导航点击
	element.on('nav(rightNav)', function (elem) {
		var navA = $(elem).find('a');
		var id = navA.attr('data-id');
		var url = navA.attr('data-url');
		var text = navA.attr('data-text');
		if (!url) {
			return;
		}
		var isActive = $('.main-layout-tab .layui-tab-title').find("li[lay-id=" + id + "]");
		if (isActive.length > 0) {
			//切换到选项卡
			element.tabChange('tab', id);
		} else {
			element.tabAdd('tab', {
				title: text,
				content: '<iframe src="' + url + '" name="iframe' + id + '" class="iframe" framborder="0" data-id="' + id + '" scrolling="auto" width="100%"  height="100%"></iframe>',
				id: id
			});
			element.tabChange('tab', id);
		}
		mainLayout.removeClass('hide-side');
	});

	//菜单隐藏显示
	hideBtn.on('click', function () {
		if (!mainLayout.hasClass('hide-side')) {
			mainLayout.addClass('hide-side');
		} else {
			mainLayout.removeClass('hide-side');
		}
	});
	//遮罩点击隐藏
	mainMask.on('click', function () {
		mainLayout.removeClass('hide-side');
	})
	$('#quit').click(function () {
		$.ajax({
			type: 'GET',
			url: HOST + '/login/logout',
			dataType: 'json',
			success: function (res) {
				if (res.logout_message == '退出成功') {
					clearCookie("session_cid");
					clearCookie('role');
					window.location.href = '../../admin/index/login.html';
				}
			}
		})
	})
});