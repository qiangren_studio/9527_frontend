layui.use(['layer', 'form', 'element', 'jquery', 'laytpl'], function () {
    var laytpl = layui.laytpl, layer = layui.layer, element = layui.element, form = layui.form, $ = layui.jquery;
    var dataDan = [], dataDuo = [], dataDanNum = null, dataDuoNum = null;
    var beginTime = new Date();
    var session_cid = getCookie('session_cid');
    if (session_cid == null) {
        location.href = "/admin/index/login.html";
    }
    function openLayer(btn, context) {
        layer.open({
            type: 1
            , title: false
            , closeBtn: false
            , area: '300px;'
            , shade: .8
            , id: 'LAY_layuipro'
            , resize: false
            , btn: btn
            , btnAlign: 'c'
            , moveType: 1
            , content: context
            , success: function (layero) {
                var btnList = layero.find('.layui-layer-btn');
                var btn1 = btnList.find('.layui-layer-btn0');
                var btn2 = btnList.find('.layui-layer-btn1');
                if (btn2) {
                    btn2.click(function () {
                        window.location.reload();
                    })
                }
                btn1.click(function () {
                    parent.window.location.reload();
                })

            }
        });
    }
    beginTime = beginTime.getTime();
    layer.msg('正在全力加载试题', { time: 1000 });
    $.post({
        url: HOST + '/exam/index',
        data: { "session_cid": session_cid },
        success: function (res) {
            var res = JSON.parse(res);
            var danScore = 0, duoScore = 0;
            dataDan = res.dan;
            dataDuo = res.duo;
            dataDanNum = res.dan_num;
            dataDuoNum = res.duo_num;
            // 模板渲染
            for (var i = 0; i < dataDan.length; i++) {
                danScore += dataDan[i].score;
            }
            for (var i = 0; i < dataDuo.length; i++) {
                duoScore += dataDuo[i].score;
            }
            var data = {
                "dataDan": dataDan,
                "dataDuo": dataDuo,
                "dataDanNum": dataDanNum,
                "dataDuoNum": dataDuoNum,
                "dataDanScore": danScore,
                "dataDuoScore": duoScore,
            }
            var getSheet = exam_sheet.innerHTML;
            var sheet = document.getElementById('sheet');
            laytpl(getSheet).render(data, function (html0) {
                sheet.innerHTML = html0;
            });
            var getTpl = exam_danxuan.innerHTML;
            var view = document.getElementById('view');
            laytpl(getTpl).render(data, function (html) {
                view.innerHTML = html;
            });
            var getTplDuo = exam_duoxuan.innerHTML;
            var duoxuan = document.getElementById('duoxuan');
            laytpl(getTplDuo).render(data, function (html2) {
                duoxuan.innerHTML = html2;
            });
            (function (e, c, a, g) {
                var d = "countDown"; var f = { css_class: "countdown", always_show_days: false, with_labels: true, with_seconds: true, with_separators: true, with_hh_leading_zero: true, with_mm_leading_zero: true, with_ss_leading_zero: true, label_dd: "days", label_hh: "", label_mm: "", label_ss: "", separator: ":", separator_days: "," }; function b(i, h) { this.element = e(i); this.options = e.extend({}, f, h); this._defaults = f; this._name = d; this.init() } e.extend(b.prototype, {
                    init: function () { if (this.element.children().length) { return } if (this.element.attr("datetime")) { this.endDate = this.parseEndDate(this.element.attr("datetime")) } if (this.endDate === g) { this.endDate = this.parseEndDate(this.element.text()) } if (this.endDate === g) { return } if (this.element.is("time")) { this.timeElement = this.element } else { this.timeElement = e("<time></time>"); this.element.html(this.timeElement) } this.markup(); this.setTimeoutDelay = this.sToMs(1); this.daysVisible = true; this.timeElement.on("time.elapsed", this.options.onTimeElapsed); this.timeElement.on("time.tick", this.options.onTick); this.doCountDown() }, parseEndDate: function (i) { var h; h = this.parseDuration(i); if (h instanceof Date) { return h } h = this.parseDateTime(i); if (h instanceof Date) { return h } h = this.parseHumanReadableDuration(i); if (h instanceof Date) { return h } h = Date.parse(i); if (!isNaN(h)) { return new Date(h) } }, parseDuration: function (o) { var l = o.match(/^P(?:(\d+)D)?T?(?:(\d+)H)?(?:(\d+)M)?(?:(\d+)(?:\.(\d{1,3}))?S)?$/); if (l) { var n, h, k, m, j, i; h = l[1] ? this.dToMs(l[1]) : 0; k = l[2] ? this.hToMs(l[2]) : 0; m = l[3] ? this.mToMs(l[3]) : 0; j = l[4] ? this.sToMs(l[4]) : 0; i = l[5] ? parseInt(l[5], 10) : 0; n = new Date(); n.setTime(n.getTime() + h + k + m + j + i); return n } }, parseDateTime: function (o) { var q = o.match(/^(\d{4,})-(\d{2})-(\d{2})[T\s](\d{2}):(\d{2})(?:\:(\d{2}))?(?:\.(\d{1,3}))?([Z\+\-\:\d]+)?$/); if (q) { var l = q[8] ? q[8].match(/^([\+\-])?(\d{2}):?(\d{2})$/) : g; var s = 0; if (l) { s = this.hToMs(l[2]) + this.mToMs(l[3]); s = (l[1] === "-") ? s : -s } var n, m, i, p, j, k, r, h; m = q[1]; i = q[2] - 1; p = q[3]; j = q[4] || 0; k = q[5] || 0; r = q[6] || 0; h = q[7] || 0; n = new Date(Date.UTC(m, i, p, j, k, r, h)); n.setTime(n.getTime() + s); return n } }, parseHumanReadableDuration: function (o) { var l = o.match(/^(?:(\d+).+\s)?(\d+)[h:]\s?(\d+)[m:]?\s?(\d+)?[s]?(?:\.(\d{1,3}))?$/); if (l) { var n, h, k, m, j, i; n = new Date(); h = l[1] ? this.dToMs(l[1]) : 0; k = l[2] ? this.hToMs(l[2]) : 0; m = l[3] ? this.mToMs(l[3]) : 0; j = l[4] ? this.sToMs(l[4]) : 0; i = l[5] ? parseInt(l[5], 10) : 0; n.setTime(n.getTime() + h + k + m + j + i); return n } }, sToMs: function (h) { return parseInt(h, 10) * 1000 }, mToMs: function (h) { return parseInt(h, 10) * 60 * 1000 }, hToMs: function (i) { return parseInt(i, 10) * 60 * 60 * 1000 }, dToMs: function (h) { return parseInt(h, 10) * 24 * 60 * 60 * 1000 }, msToS: function (h) { return parseInt((h / 1000) % 60, 10) }, msToM: function (h) { return parseInt((h / 1000 / 60) % 60, 10) }, msToH: function (h) { return parseInt((h / 1000 / 60 / 60) % 24, 10) }, msToD: function (h) { return parseInt((h / 1000 / 60 / 60 / 24), 10) }, markup: function () { var h = ['<span class="item item-dd">', '<span class="dd"></span>', '<span class="label label-dd">', this.options.label_dd, "</span>", "</span>", '<span class="separator separator-dd">', this.options.separator_days, "</span>", '<span class="item item-hh">', '<span class="hh-1"></span>', '<span class="hh-2"></span>', '<span class="label label-hh">', this.options.label_hh, "</span>", "</span>", '<span class="separator">', this.options.separator, "</span>", '<span class="item item-mm">', '<span class="mm-1"></span>', '<span class="mm-2"></span>', '<span class="label label-mm">', this.options.label_mm, "</span>", "</span>", '<span class="separator">', this.options.separator, "</span>", '<span class="item item-ss">', '<span class="ss-1"></span>', '<span class="ss-2"></span>', '<span class="label label-ss">', this.options.label_ss, "</span>", "</span>"]; this.timeElement.html(h.join("")); if (!this.options.with_labels) { this.timeElement.find(".label").remove() } if (!this.options.with_separators) { this.timeElement.find(".separator").remove() } if (!this.options.with_seconds) { this.timeElement.find(".item-ss").remove(); this.timeElement.find(".separator").last().remove() } this.item_dd = this.timeElement.find(".item-dd"); this.separator_dd = this.timeElement.find(".separator-dd"); this.remaining_dd = this.timeElement.find(".dd"); this.remaining_hh1 = this.timeElement.find(".hh-1"); this.remaining_hh2 = this.timeElement.find(".hh-2"); this.remaining_mm1 = this.timeElement.find(".mm-1"); this.remaining_mm2 = this.timeElement.find(".mm-2"); this.remaining_ss1 = this.timeElement.find(".ss-1"); this.remaining_ss2 = this.timeElement.find(".ss-2"); this.timeElement.addClass(this.options.css_class) }, doCountDown: function () {
                        var j = this.endDate.getTime() - new Date().getTime(); var l = this.msToS(j); var m = this.msToM(j); var k = this.msToH(j); var h = this.msToD(j); if (j <= 0) { l = m = k = h = 0 } this.displayRemainingTime({ "ss": l < 10 ? (this.options.with_ss_leading_zero ? "0" : " ") + l.toString() : l.toString(), "mm": m < 10 ? (this.options.with_mm_leading_zero ? "0" : " ") + m.toString() : m.toString(), "hh": k < 10 ? (this.options.with_hh_leading_zero ? "0" : " ") + k.toString() : k.toString(), "dd": h.toString() }); if (!this.options.with_seconds && h === 0 && m === 0 && k === 0) { l = 0 } if (h === 0 && m === 0 && k === 0 && l === 0) {
                            return this.timeElement.trigger("time.elapsed")
                        } var i = this; c.setTimeout(function () { i.doCountDown() }, i.setTimeoutDelay); return this.timeElement.trigger("time.tick", j)
                    }, displayRemainingTime: function (i) { var h = []; h.push("P"); if (i.dd !== "0") { h.push(i.dd, "D") } h.push("T", i.hh, "H", i.mm, "M"); if (this.options.with_seconds) { h.push(i.ss, "S") } this.timeElement.attr("datetime", h.join("")); if (this.daysVisible && !this.options.always_show_days && i.dd === "0") { this.item_dd.remove(); this.separator_dd.remove(); this.daysVisible = false } this.remaining_dd.text(i.dd); this.remaining_hh1.text(i.hh[0].trim()); this.remaining_hh2.text(i.hh[1]); this.remaining_mm1.text(i.mm[0].trim()); this.remaining_mm2.text(i.mm[1]); this.remaining_ss1.text(i.ss[0].trim()); this.remaining_ss2.text(i.ss[1]) }
                }); e.fn[d] = function (i) { var h = arguments; if (i === g || typeof i === "object") { return this.each(function () { if (!e.data(this, "plugin_" + d)) { e.data(this, "plugin_" + d, new b(this, i)) } }) } else { if (typeof i === "string" && i[0] !== "_" && i !== "init") { var j; this.each(function () { var k = e.data(this, "plugin_" + d); if (k instanceof b && typeof k[i] === "function") { j = k[i].apply(k, Array.prototype.slice.call(h, 1)) } if (i === "destroy") { e.data(this, "plugin_" + d, null) } }); return j !== g ? j : this } } }
            })($, window, document);
            $('time').countDown({
                with_separators: false
            });
            $('.alt-1').countDown({
                css_class: 'countdown-alt-1'
            });
            $('.alt-2').countDown({
                css_class: 'countdown-alt-2'
            });
            (function () { (function (a) { a.easyPieChart = function (c, k) { var e, f, h, i, j, d, b, g = this; this.el = c; this.$el = a(c); this.$el.data("easyPieChart", this); this.init = function () { var l; g.options = a.extend({}, a.easyPieChart.defaultOptions, k); l = parseInt(g.$el.data("percent"), 10); g.percentage = 0; g.canvas = a("<canvas width='" + g.options.size + "' height='" + g.options.size + "'></canvas>").get(0); g.$el.append(g.canvas); if (typeof G_vmlCanvasManager !== "undefined" && G_vmlCanvasManager !== null) { G_vmlCanvasManager.initElement(g.canvas) } g.ctx = g.canvas.getContext("2d"); g.ctx.translate(g.options.size / 2, g.options.size / 2); g.$el.addClass("easyPieChart"); g.$el.css({ width: g.options.size, height: g.options.size, lineHeight: "" + g.options.size + "px" }); g.update(l); return g }; this.update = function (l) { if (g.options.animate === false) { return h(l) } else { return f(g.percentage, l) } }; d = function () { var m, n, l; g.ctx.fillStyle = g.options.scaleColor; g.ctx.lineWidth = 1; l = []; for (m = n = 0; n <= 24; m = ++n) { l.push(e(m)) } return l }; e = function (l) { var m; m = l % 6 === 0 ? 0 : g.options.size * 0.017; g.ctx.save(); g.ctx.rotate(l * Math.PI / 12); g.ctx.fillRect(g.options.size / 2 - m, 0, -g.options.size * 0.05 + m, 1); return g.ctx.restore() }; b = function () { var l; l = g.options.size / 2 - g.options.lineWidth / 2; if (g.options.scaleColor !== false) { l -= g.options.size * 0.08 } g.ctx.beginPath(); g.ctx.arc(0, 0, l, 0, Math.PI * 2, true); g.ctx.closePath(); g.ctx.strokeStyle = g.options.trackColor; g.ctx.lineWidth = g.options.lineWidth; return g.ctx.stroke() }; j = function () { if (g.options.scaleColor !== false) { d() } if (g.options.trackColor !== false) { return b() } }; h = function (l) { var m; j(); g.ctx.strokeStyle = a.isFunction(g.options.barColor) ? g.options.barColor(l) : g.options.barColor; g.ctx.lineCap = g.options.lineCap; m = g.options.size / 2 - g.options.lineWidth / 2; if (g.options.scaleColor !== false) { m -= g.options.size * 0.08 } g.ctx.save(); g.ctx.rotate(-Math.PI / 2); g.ctx.beginPath(); g.ctx.arc(0, 0, m, 0, Math.PI * 2 * l / 100, false); g.ctx.stroke(); return g.ctx.restore() }; f = function (p, o) { var m, n, l; n = 30; l = n * g.options.animate / 1000; m = 0; g.options.onStart.call(g); g.percentage = o; if (g.animation) { clearInterval(g.animation); g.animation = false } return g.animation = setInterval(function () { g.ctx.clearRect(-g.options.size / 2, -g.options.size / 2, g.options.size, g.options.size); j.call(g); h.call(g, [i(m, p, o - p, l)]); m++; if ((m / l) > 1) { clearInterval(g.animation); g.animation = false; return g.options.onStop.call(g) } }, 1000 / n) }; i = function (m, l, o, n) { m /= n / 2; if (m < 1) { return o / 2 * m * m + l } else { return -o / 2 * ((--m) * (m - 2) - 1) + l } }; return this.init() }; a.easyPieChart.defaultOptions = { barColor: "#00ff00", trackColor: "#f2f2f2", scaleColor: "#ffffff", lineCap: "round", size: 80, lineWidth: 2, animate: false, onStart: a.noop, onStop: a.noop }; a.fn.easyPieChart = function (b) { return a.each(this, function (d, e) { var c; c = a(e); if (!c.data("easyPieChart")) { return c.data("easyPieChart", new a.easyPieChart(e, b)) } }) }; return void 0 })($) }).call(this);
            $('li.option label').click(function () {
                var examId = $(this).closest('.test_content_nr_main').closest('li').attr('id'); // 得到题目ID
                var cardLi = $('a[href="#' + examId + '"]'); // 根据题目ID找到对应答题卡
                // 设置已答题
                if (!cardLi.hasClass('hasBeenAnswer')) {
                    cardLi.addClass('hasBeenAnswer');
                }

            });
        },
        error: function (err) {
            layer.msg('服务器出错');
        }
    })
    setTimeout(function () {
        $('#examend').trigger('click');
        $('#layui-layer2').find('.layui-layer-btn1').hide();
        $('#support div').html('考试时间到，请提交试卷');
    }, 1200000);
    //监听表单提交
    form.on('submit(examend)', function (res) {
        res.field.stime = beginTime;
        res.field.session_cid = session_cid;
        console.log(res.field);
        layer.open({
            type: 1
            , title: false //不显示标题栏
            , closeBtn: false
            , area: '300px;'
            , shade: .8
            , id: 'support' //设定一个id，防止重复弹出
            , resize: false
            , btn: ['确认交卷', '再考虑一下']
            , btnAlign: 'c'
            , moveType: 1 //拖拽模式，0或者1
            , content: '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;text-align:left;">确认交卷吗？</div>'
            , success: function (layero, index) {
                var btnList = layero.find('.layui-layer-btn');
                var btn1 = btnList.find('.layui-layer-btn0');
                var btn2 = btnList.find('.layui-layer-btn1');
                btn1.click(function () {
                    $.post({
                        url: HOST + '/exam/sub',
                        data: res.field,
                        dataType: "json", //指定服务器返回的数据类型
                        success: function (res) {
                            if (res.score >= 60) {
                                var btnArr1 = ['确认'];
                                var context1 = '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;text-align:left;">恭喜您通过了考试<br><br>分数：' + res.score + '分</div>';
                                openLayer(btnArr1, context1);
                            } else {
                                var btnArr2 = ['复习一下', '再考一次'];
                                var context2 = '<div style="padding: 50px; line-height: 22px; background-color: #393D49; color: #fff; font-weight: 300;text-align:left;">抱歉您没有通过考试<br><br>分数：' + res.score + '分</div>';
                                openLayer(btnArr2, context2);
                            }
                        },
                        error: function (err) {
                            layer.msg('暂时无法提交');
                        }
                    })
                });
                btn2.click(function () {
                    return false
                })
            }
        });
        return false;
    });
});