layui.define('jquery', function (exports) {
    var $ = layui.jquery;
    var formate = {
        dataTpl: function () {
            $("[data-field='rqtype']").children().each(function () {
                if ($(this).text() == '1') {
                    $(this).text("民用");
                } else if ($(this).text() == '2') {
                    $(this).text("商户");
                }
            })
            $("[data-field='status1']").children().each(function () {
                if ($(this).text() == '1') {
                    $(this).text("已下单");
                } else if ($(this).text() == '2') {
                    $(this).text("已缴费");
                } else if ($(this).text() == '3') {
                    $(this).text("已投保");
                } else if ($(this).text() == '4') {
                    $(this).text("已撤单");
                } else if ($(this).text() == '5') {
                    $(this).text("已退费");
                }
            })
            $("[data-field='status2']").children().each(function () {
                if ($(this).text() == '1') {
                    $(this).text("代结");
                } else if ($(this).text() == '2') {
                    $(this).text("已结");
                }
            })
            $("[data-field='status']").children().each(function () {
                if ($(this).text() == '1') {
                    $(this).text("启用");
                } else if ($(this).text() == '2') {
                    $(this).text("注销");
                }
            })
            $("[data-field='registry']").children().each(function () {
                if ($(this).text() == '1') {
                    $(this).text("通过");
                } else if ($(this).text() == '2') {
                    $(this).text("未通过");
                }else if ($(this).text() == '3') {
                    $(this).text("待审核");
                }
            })
            $("[data-field='exam']").children().each(function () {
                if ($(this).text() == '1') {
                    $(this).text("通过");
                } else if ($(this).text() == '2') {
                    $(this).text("未通过");
                }
            })
        }
    };
    exports('formate', formate);
});



