layui.define('', function (exports) {
    var time_filter = {
        toDateString: function (d, format) {
            var date = new Date(d * 1000)
                , ymd = [
                    this.digit(date.getFullYear(), 4)
                    , this.digit(date.getMonth() + 1)
                    , this.digit(date.getDate())
                ]
                , hms = [
                    this.digit(date.getHours())
                    , this.digit(date.getMinutes())
                    , this.digit(date.getSeconds())
                ];

            format = format || 'yyyy-MM-dd HH:mm:ss';

            return format.replace(/yyyy/g, ymd[0])
                .replace(/MM/g, ymd[1])
                .replace(/dd/g, ymd[2])
                .replace(/HH/g, hms[0])
                .replace(/mm/g, hms[1])
                .replace(/ss/g, hms[2]);
        },
        digit: function (num, length, end) {
            var str = '';
            num = String(num);
            length = length || 2;
            for (var i = num.length; i < length; i++) {
                str += '0';
            }
            return num < Math.pow(10, length) ? str + (num | 0) : num;
        },
        toMin: function (d) {
            if (d < 60) {
                var sec = d + '秒'
                return sec;
            } else if (d >= 60) {
                var min = (d / 60).toFixed(0) + '分';
                return min;
            }
        }

    }

    // layui.laytpl.toDateString = function (d, format) {
    //     var date = new Date(d * 1000)
    //         , ymd = [
    //             this.digit(date.getFullYear(), 4)
    //             , this.digit(date.getMonth() + 1)
    //             , this.digit(date.getDate())
    //         ]
    //         , hms = [
    //             this.digit(date.getHours())
    //             , this.digit(date.getMinutes())
    //             , this.digit(date.getSeconds())
    //         ];

    //     format = format || 'yyyy-MM-dd HH:mm:ss';

    //     return format.replace(/yyyy/g, ymd[0])
    //         .replace(/MM/g, ymd[1])
    //         .replace(/dd/g, ymd[2])
    //         .replace(/HH/g, hms[0])
    //         .replace(/mm/g, hms[1])
    //         .replace(/ss/g, hms[2]);
    // };
    // //数字前置补零
    // layui.laytpl.digit = function (num, length, end) {
    //     var str = '';
    //     num = String(num);
    //     length = length || 2;
    //     for (var i = num.length; i < length; i++) {
    //         str += '0';
    //     }
    //     return num < Math.pow(10, length) ? str + (num | 0) : num;
    // };
    // layui.laytpl.toMin = function (d) {
    //     if (d < 60) {
    //         var sec = d + '秒'
    //         return sec;
    //     } else if (d >= 60) {
    //         var min = (d / 60).toFixed(0) + '分';
    //         return min;
    //     }
    // }
    //输出test接口
    exports('time_filter', time_filter);
}
);
