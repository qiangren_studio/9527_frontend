layui.define('layer', function (exports) {
    var layer = layui.layer;
    var open = {
        // id:string,content:DOM,area:array,maxmin:bool默认为true
        open: function (id,content,area,maxmin) {
            layer.open({
                type: 1,
                area: area,
                content: content,
                maxmin: maxmin || true,
                id: id,
                shadeClose: true
            });
        },
        msg:function(context,time){
            layer.msg(context,{
                time:1000 || time
            })
        }
    }
    exports('open', open);
}
);
